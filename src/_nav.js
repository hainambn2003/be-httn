// nav.js
export default function getNavItems(role) {
  const navItems = [
    {
      component: 'CNavItem',
      name: 'Trang chủ',
      to: '/dashboard',
      icon: 'cil-speedometer',
    },
    {
      component: 'CNavTitle',
      name: 'Quản lý',
    },
    ...(role === 'ROLE_ADMIN' ? [{
      component: 'CNavItem',
      name: 'Quản lý thành viên',
      to: '/management/users',
      icon: 'cil-user',
    }] : []),
    {
      component: 'CNavItem',
      name: 'Quản lý bài thi',
      to: '/test',
      icon: 'cil-pencil',
    },
    {
      component: 'CNavItem',
      name: 'Quản lý bộ câu hỏi',
      to: '/management/questions',
      icon: 'cil-puzzle',
    },
    
    
    // {
    //   component: 'CNavItem',
    //   name: 'Báo cáo thống kê',
    //   to: '/theme/mainchart',
    //   icon: 'cil-list',
    // },
    ...(role === 'ROLE_ADMIN' ? [
      {
        component: 'CNavTitle',
        name: 'Báo cáo',
      }, 
      {
      component: 'CNavItem',
      name: 'Báo cáo thống kê',
      to: '/theme/mainchart',
      icon: 'cil-list',
    }] : []),
  ];

  return navItems;
}
