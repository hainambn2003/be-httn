import axios from "axios";

axios.defaults.baseURL = "https://httn-25.onrender.com/";
axios.defaults.headers.common["Authorization"] = "Bearer " + localStorage.getItem("token");