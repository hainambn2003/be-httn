import { h, resolveComponent } from 'vue'
import { createRouter, createWebHistory  } from 'vue-router'

import DefaultLayout from '@/layouts/DefaultLayout'

const isAuthenticated = () => {
  // Thay thế điều kiện này bằng logic thực tế để kiểm tra trạng thái đăng nhập
  return !!localStorage.getItem('token'); 
};
const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/pages/Login.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/pages/Register.vue'),
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: () => import('@/views/pages/ForgotPassword.vue'),
  },
  {
    path: '/reset-password/:username/:token',
    name: 'ResetPassword',
    component: () => import('@/views/pages/ResetPassword.vue'),
    props: true
  },
  {
    path: '/',
    name: 'Trang chủ',
    component: DefaultLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('@/views/dashboard/Dashboard.vue'),
      },
      {
        path: '/change-password',
        name: 'Đổi mật khẩu',
        component: () => import('@/views/pages/ChangePassword.vue'),
      },

    //Route to component depending on link that u click
    //thiết lập link khi click để bên _nav.js điều hướng đến component
      {
        path: '/test',
        name: 'Test',
        redirect: '/test',
        children: [
          {
            path: '/test',
            name: 'Test',
            component: () => import('@/views/test/TestPage.vue'),
          },
          {
            path: '/test/create',
            name: 'AddTest',
            component: () => import('@/views/test/AddTestPage.vue'),
          },
          {
            path: '/test/detail/:id',
            name: 'DetailTest',
            component: () => import('@/views/test/DetailTestPage.vue'),
          },
          {
            path: '/test/edit/:id',
            name: 'EditTest',
            component: () => import('@/views/test/EditTestPage.vue'),
          }, 
          {
            path: '/test/quiz/:id/:number/:time',
            name: 'Quiz',
            component: () => import('@/views/quiz/QuizView.vue'),
          },
          {
            path: '/test/quiz/result/:id',
            name: 'ResultQuiz',
            component: () => import('@/views/quiz/ViewResult.vue'),
          },
        ]
      },
      {
        path: '/management/users',
        name: 'Quản lý thành viên',
        component: () => import('@/views/manage-user/ManageUser.vue'),
      },
      {
        path: '/management/questions',
        name: 'Quản lý bộ câu hỏi',
        component: () => import('@/views/manage-question/ManageQuestion.vue'),
      },
      {
        path: '/management/questions/create/:id',
        name: 'AddQuestion',
        component: () => import('@/views/manage-question/AddQuestion.vue'),
      },
      {
        path: '/theme/mainchart',
        name: 'Thống kê',
        component: () => import('@/views/dashboard/MainChart.vue'),
      },
      
    ],
  },
]

const router = createRouter({
  history: createWebHistory (process.env.BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

// Thêm logic chuyển hướng trước mỗi lần thay đổi route
router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && to.name !== 'Register' && to.name !== 'ForgotPassword' && to.name !== 'ResetPassword' && !isAuthenticated()) {
    next({ name: 'Login' });
  } else {
    next();
  }
});

export default router
